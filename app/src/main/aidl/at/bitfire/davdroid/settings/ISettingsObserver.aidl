package at.bitfire.davdroid.settings;

interface ISettingsObserver {

    void onSettingsChanged();

}
